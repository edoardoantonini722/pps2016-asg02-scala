import game.GameController;
import game.GameLoop;
import game.Platform;

import javax.swing.JFrame;

public class GameStart {

    private static final int WINDOW_WIDTH = 700;
    private static final int WINDOW_HEIGHT = 360;
    private static final String WINDOW_TITLE = "Super Mario - Refactor by Edoardo Antonini";

    public static void main(String[] args) {
        JFrame window = new JFrame(WINDOW_TITLE);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        window.setLocationRelativeTo(null);
        window.setResizable(true);
        window.setAlwaysOnTop(true);

        window.setContentPane(new Platform());
        window.setVisible(true);

        Thread timer = new Thread(new GameLoop());
        timer.start();
    }

}
