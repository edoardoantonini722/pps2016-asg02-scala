package elements

import game.{GameController, Platform}

/**
  * An interface that describes the basic game element
  */
trait BasicGameElement {
  def width: Int

  def height: Int

  def x: Int

  def y: Int

  def width_=(width: Int): Unit

  def height_=(height: Int): Unit

  def x_=(x: Int): Unit

  def y_=(y: Int): Unit

  def move(): Unit = {
    val platform = GameController.getInstance.getPlatform
    if (platform.getXPos >= Platform.STARTING_X_POS) this.x = this.x - platform.getMov
  }
}