package elements

import java.awt._

/**
  * An interface for all those elemens that need to manage an image
  */
trait GameElementWithImage {
  def objectImage: Image

  def objectImage_=(objectImage: Image): Unit
}

case class GameObject(override var x: Int,override var y: Int,override var width: Int,override var height: Int, override var objectImage : Image) extends BasicGameElement with GameElementWithImage {}