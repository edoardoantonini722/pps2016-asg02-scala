package elements

import elements.characters.{Kong, Mushroom, Samus, Turtle}
import elements.objects.Block
import elements.objects.Piece
import elements.objects.Tunnel

/**
  * The interface for abstract factory pattern
  */
trait GameElementFactory {
  def createPiece(x: Int, y: Int): Piece

  def createTunnel(x: Int, y: Int): Tunnel

  def createBlock(x: Int, y: Int): Block

  def createTurtle(x: Int, y: Int): Turtle

  def createMushroom(x: Int, y: Int): Mushroom

  def createSamus(x: Int, y: Int): Samus

  def createKong(x: Int, y: Int): Kong
}

object GameElementFactory {
  def apply(): GameElementFactory = new ConcreteGameElementFactory()

  private class ConcreteGameElementFactory extends GameElementFactory {
    override def createPiece(x: Int, y: Int) = new Piece(x, y)

    override def createTunnel(x: Int, y: Int) = new Tunnel(x, y)

    override def createBlock(x: Int, y: Int) = new Block(x, y)

    override def createTurtle(x: Int, y: Int) = new Turtle(x, y)

    override def createMushroom(x: Int, y: Int) = new Mushroom(x, y)

    override def createKong(x: Int, y: Int): Kong = new Kong(x,y)

    override def createSamus(x: Int, y: Int): Samus = new Samus(x,y)
  }
}