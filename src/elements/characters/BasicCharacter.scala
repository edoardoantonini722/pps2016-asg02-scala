package elements.characters

import java.awt.Image

import elements.BasicGameElement
import utils.Res
import utils.Utils

object BasicCharacter {
  val PROXIMITY_MARGIN = 10
  val CONTACT_MARGIN = 5
  val DIRECTION_RIGHT: Int = 1
  val DIRECTION_LEFT: Int = -1
}

abstract class BasicCharacter(override var x: Int,override var y: Int,override var width: Int,override var height: Int) extends BasicGameElement with Character {

  private var moving = false
  private var toRight = true
  private var counter = 0
  private var alive = true
  private var isWalkingImageOn = false

  def getDirection() : Int = if(isToRight) BasicCharacter.DIRECTION_RIGHT else BasicCharacter.DIRECTION_LEFT

  def getImageName: String

  def getFrequency: Int

  def contactWithElement(element: BasicGameElement): Unit
  override def getCounter: Int = counter

  override def isAlive: Boolean = alive

  override def isMoving: Boolean = moving

  override def isToRight: Boolean = toRight

  override def setAlive(alive: Boolean): Unit = this.alive = alive

  override def setMoving(moving: Boolean): Unit = this.moving = moving

  override def setToRight(toRight: Boolean): Unit = this.toRight = toRight

  override def setCounter(counter: Int): Unit = this.counter = counter

  override def walk: Image = {
    var str:String = Res.IMG_BASE + getImageName
    if ({counter += 1; counter} % getFrequency == 0)
      isWalkingImageOn = !isWalkingImageOn
    if(!isMoving || isWalkingImageOn)
      str = str concat Res.IMGP_STATUS_ACTIVE
    else
      str = str concat Res.IMGP_STATUS_NORMAL
    if (toRight)
      str = str concat Res.IMGP_DIRECTION_DX
    else
      str = str concat Res.IMGP_DIRECTION_SX
    str = str concat Res.IMG_EXT
    Utils.getImage(str)
  }


  protected def hitAhead(gameElement: BasicGameElement): Boolean =
    if (this.x + this.width < gameElement.x ||
      this.x + this.width > gameElement.x + BasicCharacter.CONTACT_MARGIN ||
      this.y + this.height <= gameElement.y ||
      this.y >= gameElement.y + gameElement.height)
      false
  else true

  protected def hitBack(gameElement: BasicGameElement): Boolean =
    if (this.x > gameElement.x + gameElement.width ||
    this.x + this.width < gameElement.x + gameElement.width - BasicCharacter.CONTACT_MARGIN ||
      this.y + this.height <= gameElement.y ||
      this.y >= gameElement.y + gameElement.height)
      false
  else true

  protected def hitBelow(gameElement: BasicGameElement): Boolean =
    if (this.x + this.width < gameElement.x + BasicCharacter.CONTACT_MARGIN ||
      this.x > gameElement.x + gameElement.width - BasicCharacter.CONTACT_MARGIN ||
      this.y + this.height < gameElement.y ||
      this.y + this.height > gameElement.y + BasicCharacter.CONTACT_MARGIN)
      false
  else true

  protected def hitAbove(gameElement: BasicGameElement): Boolean =
    if (this.x + this.width < gameElement.x + BasicCharacter.CONTACT_MARGIN ||
      this.x > gameElement.x + gameElement.width - BasicCharacter.CONTACT_MARGIN ||
      this.y < gameElement.y + gameElement.height ||
      this.y > gameElement.y + gameElement.height + BasicCharacter.CONTACT_MARGIN)
      false
  else true

  def isNearby(gameElement: BasicGameElement): Boolean = {
    if ((this.x > gameElement.x - BasicCharacter.PROXIMITY_MARGIN &&
      this.x < gameElement.x + gameElement.width + BasicCharacter.PROXIMITY_MARGIN) ||
      (this.x + this.width > gameElement.x - BasicCharacter.PROXIMITY_MARGIN &&
        this.x + this.width < gameElement.x + gameElement.width + BasicCharacter.PROXIMITY_MARGIN))
       true
    else false
  }

}