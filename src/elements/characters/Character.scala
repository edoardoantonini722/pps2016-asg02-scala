package elements.characters

import elements.GameElementWithImage
import java.awt.Image

/**
  * Interface that specifies the basic character functions
  */
trait Character extends GameElementWithImage {
  def walk: Image

  def getCounter: Int

  def isAlive: Boolean

  def isMoving: Boolean

  def isToRight: Boolean

  def setAlive(alive: Boolean): Unit

  def setMoving(moving: Boolean): Unit

  def setToRight(toRight: Boolean): Unit

  def setCounter(counter: Int): Unit
}