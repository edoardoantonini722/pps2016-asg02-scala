package elements.characters

import elements.BasicGameElement
import java.awt._

object EnemyCharacter {
  val PAUSE = 15
}

abstract class EnemyCharacter(var x1: Int, y: Int, width: Int, height: Int, override var objectImage: Image) extends BasicCharacter(x1, y, width, height) with Runnable {
  private var direction = 0

  override def move(): Unit = {

    direction = if (isToRight) BasicCharacter.DIRECTION_RIGHT else BasicCharacter.DIRECTION_LEFT
    if(isAlive) {
        x = x + direction
    }else{
      super.move()
    }
  }

  override def contactWithElement(obj: BasicGameElement): Unit = if (this.hitAhead(obj) && this.isToRight) {
    setToRight(false)
    direction = BasicCharacter.DIRECTION_LEFT
  }
  else if (this.hitBack(obj) && !this.isToRight) {
    setToRight(true)
    direction = BasicCharacter.DIRECTION_RIGHT
  }

  def setDirection(direction: Int): Unit = this.direction = direction

  override def getDirection: Int = direction

  def getDeadOffsetY: Int

  def deadImage: Image

  override def run(): Unit = {
    while (isAlive) {
      this.move()
      try
        Thread.sleep (EnemyCharacter.PAUSE)
      catch {
        case e: InterruptedException => e.printStackTrace ()
      }
    }
  }
}