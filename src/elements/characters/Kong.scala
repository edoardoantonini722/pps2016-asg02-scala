package elements.characters

import java.awt.Image

import utils.{Res, Utils}

object Kong{
  private val KONG_DEAD_OFFSET_Y = 0
  private val WIDTH = 70
  private val HEIGHT = 50
  private val KONG_FREQUENCY = 45
  private val KONG_IMAGE=Utils.getImage(Res.IMG_KONG_DEFAULT)
}

class Kong(val x2:Int, val y2: Int) extends EnemyCharacter(x2,y2,Kong.WIDTH,Kong.HEIGHT,Kong.KONG_IMAGE) with Runnable{
  setToRight(true)
  setMoving(true)

  this.setDirection(BasicCharacter.DIRECTION_RIGHT)
  val chronoKong = new Thread(this)
  chronoKong.start()

  override def deadImage: Image = Utils.getImage(Res.IMG_KONG_DEAD)

  override def getImageName: String = Res.IMGP_CHARACTER_KONG

  override def getFrequency: Int = Kong.KONG_FREQUENCY

  override def getDeadOffsetY: Int = Kong.KONG_DEAD_OFFSET_Y

}

