package elements.characters

import java.awt.Image

import elements.{BasicGameElement, GameObject}
import game.GameController
import elements.objects.Piece
import utils.Res
import utils.Utils

object Mario {
  private val MARIO_FREQUENCY = 25
  private val MARIO_INIITIAL_X_POSITION = 300
  private val MARIO_INITIAL_Y_POSITION = 245
  private val MARIO_OFFSET_Y_INITIAL = 243
  private val FLOOR_OFFSET_Y_INITIAL = 293
  private val WIDTH = 28
  private val HEIGHT = 50
  private val JUMPING_LIMIT = 42
  private val HEIGHT_REDUCTION = 4
  private val IMAGE = Utils.getImage(Res.IMG_MARIO_DEFAULT)
  private var mario: Mario = null

  def getInstance: Mario = {
    if (mario == null) mario =  new Mario(MARIO_INIITIAL_X_POSITION, MARIO_INITIAL_Y_POSITION,IMAGE)
    mario
  }
}

class Mario private( var x1: Int,var y1: Int, override var objectImage :Image) extends BasicCharacter(x1, y1, Mario.WIDTH, Mario.HEIGHT) {

  private var jumping = false
  private var jumpingExtent = 0

  def isJumping: Boolean = jumping

  def setJumping(jumping: Boolean): Unit = this.jumping = jumping

  def doJump: Image = {
    var str: String = null
    this.jumpingExtent += 1
    val platform = GameController.getInstance.getPlatform
    if (jumpingExtent < Mario.JUMPING_LIMIT) {
      if (this.y > platform.getSkyLimit)
        this.y = this.y - Mario.HEIGHT_REDUCTION
      else
        jumpingExtent = Mario.JUMPING_LIMIT
      str = if (isToRight) Res.IMG_MARIO_SUPER_DX else Res.IMG_MARIO_SUPER_SX
    }
    else if (this.y + this.height < platform.getFloorOffsetY) {
      this.y = this.y + 1
      str = if (isToRight) Res.IMG_MARIO_SUPER_DX else Res.IMG_MARIO_SUPER_SX
    }
    else {
      str = if (isToRight) Res.IMG_MARIO_ACTIVE_DX else Res.IMG_MARIO_ACTIVE_SX
      jumping = false
      jumpingExtent = 0
    }
    Utils.getImage(str)
  }



  override def contactWithElement(element: BasicGameElement): Unit = element match {
    case samus : Samus => contactSamus(samus.asInstanceOf[Samus])
    case enemy : EnemyCharacter => contactEnemy(enemy.asInstanceOf[EnemyCharacter])
    case fixedObject : GameObject => contactObject(fixedObject.asInstanceOf[GameObject])
  }

  private def contactObject(obj: GameObject) = {
    val platform = GameController.getInstance.getPlatform
    if (this.hitAhead(obj) && this.isToRight || this.hitBack(obj) && !this.isToRight) {
      platform.setMov(0)
      setMoving(false)
    }
    if (this.hitBelow(obj) && this.jumping)
      platform.setFloorOffsetY(obj.y)
    else if (!this.hitBelow(obj)) {
      platform.setFloorOffsetY(Mario.FLOOR_OFFSET_Y_INITIAL)
      if (!this.jumping)
        this.y = Mario.MARIO_OFFSET_Y_INITIAL
      if (hitAbove(obj))
        platform.setSkyLimit(obj.y + obj.height)
      else if (!this.hitAbove(obj) && !this.jumping)
        platform.setSkyLimit(0)
    }
  }

  def contactPiece(piece: Piece): Boolean = this.hitBack(piece) || this.hitAbove(piece) || this.hitAhead(piece) || this.hitBelow(piece)

  private def contactGenericEnemy(enemyCharacter: EnemyCharacter, deadlyCondition: (EnemyCharacter) =>  Boolean, killingCondition: (EnemyCharacter) => Boolean) =
  {
    if (deadlyCondition.apply(enemyCharacter)) {
      if (enemyCharacter.isAlive) {
        this.setMoving(false)
        this.setAlive(false)
      }
      else this.setAlive(true)
    }
    else if (killingCondition.apply(enemyCharacter)) {
      enemyCharacter.setMoving(false)
      enemyCharacter.setAlive(false)
    }
  }


  private def contactEnemy(enemyCharacter: EnemyCharacter) = contactGenericEnemy(enemyCharacter,
                                                                                enemy => this.hitAhead(enemy) || this.hitBack(enemy),
                                                                                enemy => this.hitBelow(enemy)
                                                                                )
  private def contactSamus(samus: Samus) = contactGenericEnemy(samus,
                                                              samus => this.hitBack(samus) ||
                                                                        this.hitAhead(samus) &&
                                                                        (samus.getDirection != this.getDirection),
                                                              samus => this.isAlive &&
                                                                        this.hitAhead(samus) &&
                                                                        (samus.getDirection == this.getDirection)
                                                              )


  override def getImageName: String = Res.IMGP_CHARACTER_MARIO

  override def getFrequency: Int = Mario.MARIO_FREQUENCY
}