package elements.characters

import java.awt.Image
import utils.Res
import utils.Utils

object Mushroom {
  private val MUSHROOM_DEAD_OFFSET_Y = 20
  private val WIDTH = 27
  private val HEIGHT = 30
  private val MUSHROOM_FREQUENCY = 45
  private val IMAGE = Utils.getImage(Res.IMG_MUSHROOM_DEFAULT)
}

class Mushroom(val x2: Int,val y2: Int) extends EnemyCharacter(x2, y2, Mushroom.WIDTH, Mushroom.HEIGHT,objectImage = Mushroom.IMAGE) with Runnable {
  setToRight(true)
  setMoving(true)

  val chronoMushroom = new Thread(this)
  chronoMushroom.start()

  override def deadImage: Image =
    Utils.getImage(
      if (isToRight) Res.IMG_MUSHROOM_DEAD_DX
      else Res.IMG_MUSHROOM_DEAD_SX
    )
  override def getImageName: String = Res.IMGP_CHARACTER_MUSHROOM

  override def getFrequency: Int = Mushroom.MUSHROOM_FREQUENCY

  override def getDeadOffsetY: Int = Mushroom.MUSHROOM_DEAD_OFFSET_Y
}