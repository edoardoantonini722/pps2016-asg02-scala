package elements.characters

import java.awt.Image

import utils.{Res, Utils}

object Samus{
  private val SAMUS_DEAD_OFFSET_Y = 0
  private val WIDTH = 46
  private val HEIGHT = 50
  private val SAMUS_FREQUENCY = 15
  private val SAMUS_IMAGE=Utils.getImage(Res.IMG_SAMUS_DEFAULT)
}

class Samus(val x2:Int, val y2: Int) extends EnemyCharacter(x2,y2,Samus.WIDTH,Samus.HEIGHT,Samus.SAMUS_IMAGE) with Runnable{
  setToRight(true)
  setMoving(true)

  this.setDirection(BasicCharacter.DIRECTION_RIGHT)
  val chronoSamus = new Thread(this)
  chronoSamus.start()

  override def deadImage: Image = Utils.getImage(Res.IMG_SAMUS_DEAD)

  override def getImageName: String = Res.IMGP_CHARACTER_SAMUS

  override def getFrequency: Int = Samus.SAMUS_FREQUENCY

  override def getDeadOffsetY: Int = Samus.SAMUS_DEAD_OFFSET_Y

}
