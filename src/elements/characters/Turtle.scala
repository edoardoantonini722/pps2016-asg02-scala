package elements.characters

import java.awt.Image

import utils.Res
import utils.Utils

object Turtle {
  private val TURTLE_DEAD_OFFSET_Y = 30
  private val WIDTH = 43
  private val HEIGHT = 50
  private val TURTLE_FREQUENCY = 45
  private val IMAGE = Utils.getImage(Res.IMG_TURTLE_IDLE)
}

class Turtle(val x2: Int,val y2: Int) extends EnemyCharacter(x2, y2, Turtle.WIDTH, Turtle.HEIGHT,Turtle.IMAGE) with Runnable {
  setToRight(true)
  setMoving(true)

  this.setDirection(BasicCharacter.DIRECTION_RIGHT)
  val chronoTurtle = new Thread(this)
  chronoTurtle.start()

  override def deadImage: Image = Utils.getImage(Res.IMG_TURTLE_DEAD)

  override def getImageName: String = Res.IMGP_CHARACTER_TURTLE

  override def getFrequency: Int = Turtle.TURTLE_FREQUENCY

  override def getDeadOffsetY: Int = Turtle.TURTLE_DEAD_OFFSET_Y

}