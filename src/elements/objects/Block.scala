package elements.objects

import elements.GameObject
import utils.Res
import utils.Utils

object Block {
  val WIDTH = 30
  val HEIGHT = 30
  private val IMAGE = Utils.getImage(Res.IMG_BLOCK)
}

class Block(val x1: Int,val y1: Int) extends GameObject(x1, y1, Block.WIDTH, Block.HEIGHT,Block.IMAGE) {}