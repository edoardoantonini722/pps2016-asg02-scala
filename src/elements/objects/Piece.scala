package elements.objects

import utils.Res
import utils.Utils
import java.awt.Image

import elements.GameObject

object Piece {
  val WIDTH = 30
  val HEIGHT = 30
  private val PAUSE = 10
  private val FLIP_FREQUENCY = 100
  private val FLIP_COMPLETED = 0
  private val IMAGE =  Utils.getImage(Res.IMG_PIECE1)
}

class Piece(val x1: Int,val y1: Int) extends GameObject(x1, y1, Piece.WIDTH, Piece.HEIGHT, Piece.IMAGE) with Runnable {

  private var counter = 0

  def imageOnMovement: Image = Utils.getImage(if ( {
    this.counter += 1; this.counter
  } % Piece.FLIP_FREQUENCY == Piece.FLIP_COMPLETED) Res.IMG_PIECE1
  else Res.IMG_PIECE2)

  override def run(): Unit = {
    while (true){
      this.imageOnMovement
      try Thread.sleep(Piece.PAUSE)
      catch {
        case e: InterruptedException =>
      }
    }
  }
}