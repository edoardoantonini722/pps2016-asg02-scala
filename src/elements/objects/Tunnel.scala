package elements.objects

import elements.GameObject
import utils.Res
import utils.Utils

object Tunnel {
  val WIDTH = 43
  val HEIGHT = 65
  private val IMAGE = Utils.getImage(Res.IMG_TUNNEL)
}

class Tunnel(val x1: Int,val y1: Int) extends GameObject(x1, y1, Tunnel.WIDTH, Tunnel.HEIGHT, Tunnel.IMAGE) {}