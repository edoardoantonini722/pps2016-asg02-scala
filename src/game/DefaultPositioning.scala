package game


import elements.GameElementFactory
import elements.characters.EnemyCharacter
import elements.objects._

object DefaultPositioning {
  private var instance:PositioningStrategy = null
  def getInstance: PositioningStrategy = {
    if (instance == null) instance = new DefaultPositioning(Seq[Block](),Seq[EnemyCharacter](),Seq[Tunnel](),Seq[Piece]())
    instance
  }
}

class DefaultPositioning private(override var blocks: Seq[Block], override var enemies: Seq[EnemyCharacter], override var tunnels: Seq[Tunnel], override var pieces: Seq[Piece]) extends PositioningStrategy {

  val factory = GameElementFactory()
  tunnels = tunnels :+ factory.createTunnel(600, 230)
  tunnels = tunnels :+ factory.createTunnel(1000, 230)
  tunnels = tunnels :+ factory.createTunnel(1600, 230)
  tunnels = tunnels :+ factory.createTunnel(1900, 230)
  tunnels = tunnels :+ factory.createTunnel(2500, 230)
  tunnels = tunnels :+ factory.createTunnel(3000, 230)
  tunnels = tunnels :+ factory.createTunnel(3800, 230)
  tunnels = tunnels :+ factory.createTunnel(4500, 230)
  blocks = blocks :+ factory.createBlock(400, 180)
  blocks = blocks :+ factory.createBlock(1200, 180)
  blocks = blocks :+ factory.createBlock(1270, 170)
  blocks = blocks :+ factory.createBlock(1340, 160)
  blocks = blocks :+ factory.createBlock(2000, 180)
  blocks = blocks :+ factory.createBlock(2600, 160)
  blocks = blocks :+ factory.createBlock(2650, 180)
  blocks = blocks :+ factory.createBlock(3500, 160)
  blocks = blocks :+ factory.createBlock(3550, 140)
  blocks = blocks :+ factory.createBlock(4000, 170)
  blocks = blocks :+ factory.createBlock(4200, 200)
  blocks = blocks :+ factory.createBlock(4300, 210)
  pieces = pieces :+ factory.createPiece(402, 145)
  pieces = pieces :+ factory.createPiece(1202, 140)
  pieces = pieces :+ factory.createPiece(1272, 95)
  pieces = pieces :+ factory.createPiece(1342, 40)
  pieces = pieces :+ factory.createPiece(1650, 145)
  pieces = pieces :+ factory.createPiece(2650, 145)
  pieces = pieces :+ factory.createPiece(3000, 135)
  pieces = pieces :+ factory.createPiece(3400, 125)
  pieces = pieces :+ factory.createPiece(4200, 145)
  pieces = pieces :+ factory.createPiece(4600, 40)
  enemies = enemies :+ factory.createMushroom(800, 263)
  enemies = enemies :+ factory.createTurtle(950, 243)
  enemies = enemies :+ factory.createKong(2600,243)
  enemies = enemies :+ factory.createSamus(3100,243)

}