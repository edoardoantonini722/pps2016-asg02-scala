package game;

public class GameLoop implements Runnable {

    private final int PAUSE = 3;

    public void run() {
        while (true) {
            GameController.getInstance().playGame();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
            }
        }
    }

} 
