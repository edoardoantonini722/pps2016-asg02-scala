package game

import elements.characters.EnemyCharacter
import elements.objects.Block
import elements.objects.Piece
import elements.objects.Tunnel

/**
  * The interface for the strategy pattern
  */
trait PositioningStrategy {
  var tunnels : Seq[Tunnel]

  var blocks : Seq[Block]

  var pieces : Seq[Piece]

  var enemies : Seq[EnemyCharacter]
}
